/* File: ast_decl.cc
 * -----------------
 * Implementation of Decl node classes.
 */
#include <string>
#include <sstream>
#include "ast_decl.h"
#include "ast_type.h"
#include "ast_stmt.h"
#include "errors.h"
         
Decl::Decl(Identifier *n) : Node(*n->GetLocation()) {
    Assert(n != NULL);
    (id=n)->SetParent(this); 
}

void Decl::printout(){
  Iterator<Decl*> iter = scope->GetIterator();
  Decl* decl;
  while((decl = iter.GetNextValue())){
    std::cout << decl->getID() << " ";
  }
  std::cout << std::endl;
}

VarDecl::VarDecl(Identifier *n, Type *t) : Decl(n) {
    Assert(n != NULL && t != NULL);
    (type=t)->SetParent(this);
    scope = new Hashtable<Decl *>();
}

void VarDecl::Check(Hashtable<Decl*> * & parentScope){
  if(!checked){
    checked = true;
    if(type->getType() == namedType){
      Decl* decl;
      Hashtable<Decl*> * cScope = parentScope;
      while(cScope != NULL){
	decl = cScope->Lookup(type->checkType());
	if(decl == NULL){
	  cScope = cScope->getParent();
	} else {
	  break;
	}
      }
      if(decl){
	if(decl->getType() != classDecl && decl->getType() != interfaceDecl ){
	  ReportError::IdentifierNotDeclared(type->getid(), LookingForType);
	}
      } else {
	ReportError::IdentifierNotDeclared(type->getid(), LookingForType);
      }
    }
  }
}

bool VarDecl::equalType(Type* other){
 return type->IsEquivalentTo(other);
}

bool ClassDecl::equalType(Type* other){
  if(this->typeCheck()->IsEquivalentTo(other)){
    return true;
  } else if (extends){
    if(extends->IsEquivalentTo(other)){
      return true;
    }
  } else {
    for(int i = 0; i < implements->NumElements(); i++){
      if(implements->Nth(i)->IsEquivalentTo(other)){
	return true;
      }
    }
  }
  return false;
}

ClassDecl::ClassDecl(Identifier *n, NamedType *ex, List<NamedType*> *imp, List<Decl*> *m) : Decl(n) {
    // extends can be NULL, impl & mem may be empty lists but cannot be NULL
    Assert(n != NULL && imp != NULL && m != NULL);     
    extends = ex;
    if (extends) extends->SetParent(this);
    (implements=imp)->SetParentAll(this);
    (members=m)->SetParentAll(this);
    scope = new Hashtable<Decl *>();
}

void ClassDecl::Check(Hashtable<Decl*> * & parentScope){
  if(!checked){
    checked = true;
    scope->setParent(parentScope);
    scope->Enter("this", this);    
    //Set Parent Class Scope
    Hashtable<Decl *> * exScope = NULL;
    //Check on Parent Class
    if(extends){
      extends->Check(parentScope, 1);
      Decl* decl;
      Hashtable<Decl*> * cScope = parentScope;
      bool found = false;
      while(cScope != NULL){
	decl = cScope->Lookup(extends->checkType());
	if(decl != NULL){
	  found = true;
	  break;
	} else {
	  cScope = cScope->getParent();
	}
      }
      if(found){
	decl->Check(parentScope);
	scope->Enter("extends", decl);
	exScope = decl->scope;
	Iterator<Decl *> iter = exScope->GetIterator();
	//add all vardecl from parents to this scope.
	while((decl = iter.GetNextValue())){
	  if(decl->scope->NumEntries() == 0){
	    scope->Enter(decl->getID(), decl);
	  }
	}
      } 
    }
    List<Hashtable<Decl *> *> * intScope = new List<Hashtable<Decl *> *>();
    //Check on Interfaces
    for(int i = 0; i < implements->NumElements(); i++){
      implements->Nth(i)->Check(parentScope, 2);
      Decl* decl;
      bool found = false;
      Hashtable<Decl*> * cScope = parentScope;
      while(cScope != NULL){
	decl = cScope->Lookup(implements->Nth(i)->checkType());
	if(decl){
	  found = true;
	  break;
	} else {
	  cScope = cScope->getParent();
	}
      }
      if(found){
	decl->Check(parentScope);
	if(decl->getType() != interfaceDecl){
	  ReportError::IdentifierNotDeclared(implements->Nth(i)->getid(), LookingForInterface);
	} else {
	  intScope->Append(decl->scope);
	}
      }
    }
    
    for (int i = 0; i < members->NumElements(); i++){
      Decl* member = members->Nth(i);
      Decl* lookup;
      if((lookup = scope->Lookup(member->getID())) == NULL){
	scope->Enter(member->getID(), member);
      } else {
	ReportError::DeclConflict(member, lookup);
      }
    }
    
    //Check on scope
    Iterator<Decl *> iter = scope->GetIterator();
    Decl* decl;
    while((decl = iter.GetNextValue())){
      decl->Check(scope);
      //Check for override in extending class
      if(exScope != NULL){
	Decl* override;
	if((override = exScope->Lookup(decl->getID())) != NULL){
	  //Well if scope has 0 entries, it means overriding decl is VarDecl
	  //because VarDecl has no scope
	  if(override->scope->NumEntries() > 0){
	    if(override->scope->NumEntries() != decl->scope->NumEntries()){
	      ReportError::OverrideMismatch(decl);
	    }	  
	  } 
	}
      }
      //Check for Interface
      if(intScope->NumElements() > 0){
	for(int i = 0; i < intScope->NumElements(); i++){
	  Decl* interface;
	  if((interface = intScope->Nth(i)->Lookup(decl->getID()))){
	    if(interface->scope->NumEntries() != decl->scope->NumEntries()){
	      ReportError::OverrideMismatch(decl);
	    }
	  }
	}
      }
    }
    for(int i = 0; i < intScope->NumElements(); i++){
      Hashtable<Decl*> * implement = intScope->Nth(i);
      Iterator<Decl*> iter = implement->GetIterator();
      Decl* interface;
      while((interface = iter.GetNextValue())){
	if(scope->Lookup(interface->getID()) == NULL){
	  ReportError::InterfaceNotImplemented(this, implements->Nth(i));
	}
      }
    }
  }
}

Type* ClassDecl::typeCheck(){
  NamedType * type = new NamedType(id);
  return type;
}

InterfaceDecl::InterfaceDecl(Identifier *n, List<Decl*> *m) : Decl(n) {
    Assert(n != NULL && m != NULL);
    (members=m)->SetParentAll(this);
    scope = new Hashtable<Decl *>();
}

void InterfaceDecl::Check(Hashtable<Decl*> * & parentScope) {
  if(!checked){
    checked=true;
    scope->setParent(parentScope);
    for(int i = 0; i < members->NumElements(); i++){
      Decl* member = members->Nth(i);
      Decl* lookup;
      if((lookup = scope->Lookup(member->getID())) == NULL){
	scope->Enter(member->getID(), member);
      } else {
	ReportError::DeclConflict(member, lookup);
      }
    }
  }
}	
FnDecl::FnDecl(Identifier *n, Type *r, List<VarDecl*> *d) : Decl(n) {
    Assert(n != NULL && r!= NULL && d != NULL);
    (returnType=r)->SetParent(this);
    (formals=d)->SetParentAll(this);
    body = NULL;
    scope = new Hashtable<Decl *>();
}

void FnDecl::SetFunctionBody(Stmt *b) { 
    (body=b)->SetParent(this);
}

void FnDecl::Check(Hashtable<Decl*> * & parentScope) {
  if(!checked){
    checked = true;
    scope->setParent(parentScope);
    for(int i = 0; i < formals->NumElements(); i++){
      Decl* decl = formals->Nth(i);
      Decl* lookup;
      std::stringstream ss;
      ss << i;
      const char* ith = ss.str().c_str();
      if((lookup = scope->Lookup(decl->getID())) == NULL) {
	scope->Enter(decl->getID(), decl);
	scope->Enter(ith, decl);
      } else {
	ReportError::DeclConflict(decl, lookup);
      }    
    }
    //adding return type
    struct yyltype temploc;
    Identifier* rName = new Identifier(temploc, "return");
    Decl * rType = new VarDecl(rName, this->returnType);
    // adding type;
    scope->Enter("return", rType);
    Iterator<Decl *> iter = scope->GetIterator();
    Decl* decl;
    while((decl = iter.GetNextValue()) != NULL){
      decl->Check(scope);
    }
    body->Check(scope);
  }
}
