/* File: ast_stmt.cc
 * -----------------
 * Implementation of statement node classes.
 */
#include "ast_stmt.h"
#include "ast_type.h"
#include "ast_decl.h"
#include "ast_expr.h"
#include "hashtable.h"
#include "errors.h"

Program::Program(List<Decl*> *d) {
    Assert(d != NULL);
    (decls=d)->SetParentAll(this);
    scope = new Hashtable<Decl *>();
}

void Program::Check() {
    /* pp3: here is where the semantic analyzer is kicked off.
     *      The general idea is perform a tree traversal of the
     *      entire program, examining all constructs for compliance
     *      with the semantic rules.  Each node can have its own way of
     *      checking itself, which makes for a great use of inheritance
     *      and polymorphism in the node classes.
     */
  for(int i = 0; i < decls->NumElements(); i++){
    Decl* decl = decls->Nth(i);
    Decl* lookup;
    if((lookup = scope->Lookup(decl->getID())) == NULL){
      scope->Enter(decl->getID(), decl);
      decl->scope->setParent(scope);
    } else {
      ReportError::DeclConflict(decl, lookup);
    }
  }
  Iterator<Decl *> iter = scope->GetIterator();
  Decl* decl;
  while((decl = iter.GetNextValue()) != NULL){
    if(!(decl->checked)){
      decl->Check(scope);
    } 
  }
}

StmtBlock::StmtBlock(List<VarDecl*> *d, List<Stmt*> *s) {
    Assert(d != NULL && s != NULL);
    (decls=d)->SetParentAll(this);
    (stmts=s)->SetParentAll(this);
    scope = new Hashtable<Decl *>();
}

void StmtBlock::Check(Hashtable<Decl *> * & parentScope) {
  scope->setParent(parentScope);
  for(int i = 0; i < decls->NumElements(); i++){
    Decl* decl = decls->Nth(i);
    Decl* lookup;
    if((lookup = scope->Lookup(decl->getID())) == NULL){
      scope->Enter(decl->getID(), decl);
    } else {
      ReportError::DeclConflict(decl, lookup);
    }
  }
  Iterator<Decl *> iter = scope->GetIterator();
  Decl* decl;
  while((decl = iter.GetNextValue()) != NULL){
    decl->Check(scope);
  }
  for(int i = 0; i < stmts->NumElements(); i++){
    Stmt* stmt = stmts->Nth(i);
    stmt->Check(scope);
  }
}

ConditionalStmt::ConditionalStmt(Expr *t, Stmt *b) { 
    Assert(t != NULL && b != NULL);
    (test=t)->SetParent(this); 
    (body=b)->SetParent(this);
}

void ConditionalStmt::Check(Hashtable<Decl *> * & parentScope) {
  test->Check(parentScope);
  body->Check(parentScope);
}

ForStmt::ForStmt(Expr *i, Expr *t, Expr *s, Stmt *b): LoopStmt(t, b) { 
    Assert(i != NULL && t != NULL && s != NULL && b != NULL);
    (init=i)->SetParent(this);
    (step=s)->SetParent(this);
}

void ForStmt::Check(Hashtable<Decl *> * & parentScope){
  Type *iType, *tType, *sType;
  if(init){
    iType = init->typeCheck(parentScope);
  }
  tType = test->typeCheck(parentScope);
  if(!tType->IsEquivalentTo(Type::boolType)){
    ReportError::TestNotBoolean(test);
  }
  body->Check(parentScope);
}

IfStmt::IfStmt(Expr *t, Stmt *tb, Stmt *eb): ConditionalStmt(t, tb) { 
    Assert(t != NULL && tb != NULL); // else can be NULL
    elseBody = eb;
    if (elseBody) elseBody->SetParent(this);
}

void IfStmt::Check(Hashtable<Decl *> * & parentScope){
  if(!test->typeCheck(parentScope)->IsEquivalentTo(Type::boolType)){
    ReportError::TestNotBoolean(test);
  }
  body->Check(parentScope);
  if(elseBody != NULL){
    elseBody->Check(parentScope);
  }
}

ReturnStmt::ReturnStmt(yyltype loc, Expr *e) : Stmt(loc) { 
    Assert(e != NULL);
    (expr=e)->SetParent(this);
}

void ReturnStmt::Check(Hashtable<Decl *> * & parentScope) {
  Decl * rType;
  Hashtable<Decl * > * cScope = parentScope;
  while(cScope != NULL){
    rType = cScope->Lookup("return");
    if(rType) {
      Type * rtype = rType->typeCheck();
      if(rtype->IsEquivalentTo(Type::voidType)){
	// No return for void statement
	ReportError::ReturnMismatch(this, expr->typeCheck(parentScope), rtype);
      } else {
	Type * rtype2 = expr->typeCheck(parentScope);
	if(rtype2->IsEquivalentTo(Type::nullType)){
	  //NULL can be passed?
	} else if(!rtype->IsEquivalentTo(rtype2)){
	  ReportError::ReturnMismatch(this, expr->typeCheck(parentScope), rtype);
	}
      }
      break;
    } else {
      cScope = cScope->getParent();
    }
  }
  //Well we got different problem if we can actually reach this point...
}
  
PrintStmt::PrintStmt(List<Expr*> *a) {    
    Assert(a != NULL);
    (args=a)->SetParentAll(this);
}

void PrintStmt::Check(Hashtable<Decl *> * & parentScope){
  for(int i = 0; i < args->NumElements(); i++){
    Type * a = args->Nth(i)->typeCheck(parentScope);
    if(!(a->IsEquivalentTo(Type::stringType) ||
	 a->IsEquivalentTo(Type::boolType) ||
	 a->IsEquivalentTo(Type::intType) ||
	 a->IsEquivalentTo(Type::errorType))){
      ReportError::PrintArgMismatch(args->Nth(i), i+1, a);
    }
  }
}
