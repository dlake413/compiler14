/* File: ast_type.cc
 * -----------------
 * Implementation of type node classes.
 */
#include "ast_type.h"
#include "ast_decl.h"
#include "errors.h"
#include <string.h>
using namespace std;
 
/* Class constants
 * ---------------
 * These are public constants for the built-in base types (int, double, etc.)
 * They can be accessed with the syntax Type::intType. This allows you to
 * directly access them and share the built-in types where needed rather that
 * creates lots of copies.
 */

Type *Type::intType    = new Type("int");
Type *Type::doubleType = new Type("double");
Type *Type::voidType   = new Type("void");
Type *Type::boolType   = new Type("bool");
Type *Type::nullType   = new Type("null");
Type *Type::stringType = new Type("string");
Type *Type::errorType  = new Type("error"); 

Type::Type(const char *n) {
    Assert(n);
    typeName = strdup(n);
}

NamedType::NamedType(Identifier *i) : Type(*i->GetLocation()) {
    Assert(i != NULL);
    (id=i)->SetParent(this);
} 

void NamedType::Check(Hashtable<Decl*> * & parentScope, int type) {
  Decl* classIdent;
  Hashtable<Decl*> * currentScope = parentScope;
  bool found = false;
  //cout << "Searching for the " << id->getID() << endl;
  while(currentScope != NULL){
    if((classIdent = currentScope->Lookup(id->getID())) != NULL){
      //  cout << "Found!" << endl;
      found = true;
      break;
    } else {
      Iterator<Decl *> iter = currentScope->GetIterator();
      Decl* decl;
      /*
      while((decl = iter.GetNextValue()) != NULL){
	cout << "Decl: " << decl << endl;
      }
      cout << "Moving to Parent Scope" << endl;
      */
      currentScope = currentScope->getParent();
    }
  }
  if(!found){
    reasonT reason;
    switch(type){
    case 0: reason = LookingForType; break;
    case 1: reason = LookingForClass; break;
    case 2: reason = LookingForInterface; break;
    }
    ReportError::IdentifierNotDeclared(id, reason);
  }
}

ArrayType::ArrayType(yyltype loc, Type *et) : Type(loc) {
    Assert(et != NULL);
    (elemType=et)->SetParent(this);
}

void ArrayType::Check(Hashtable<Decl*> * & parentScope, int type){
  elemType->Check(parentScope, type);
}
