
/* File: ast_expr.cc
 * -----------------
 * Implementation of expression node classes.
 */
#include "ast_expr.h"
#include "ast_type.h"
#include "ast_decl.h"
#include "errors.h"
#include <string.h>
#include <sstream>
void Expr::Check(Hashtable<Decl*> * &scope){
  this->typeCheck(scope);
}

IntConstant::IntConstant(yyltype loc, int val) : Expr(loc) {
    value = val;
}

Type* IntConstant::typeCheck(Hashtable<Decl*> * &scope){
  return Type::intType;
}

DoubleConstant::DoubleConstant(yyltype loc, double val) : Expr(loc) {
    value = val;
}

Type* DoubleConstant::typeCheck(Hashtable<Decl*> * &scope){
  return Type::doubleType;
}

BoolConstant::BoolConstant(yyltype loc, bool val) : Expr(loc) {
    value = val;
}

Type* BoolConstant::typeCheck(Hashtable<Decl*> * &scope){
  return Type::boolType;
}

StringConstant::StringConstant(yyltype loc, const char *val) : Expr(loc) {
    Assert(val != NULL);
    value = strdup(val);
}

Type* StringConstant::typeCheck(Hashtable<Decl*> * &scope){
  return Type::stringType;
}

Type* NullConstant::typeCheck(Hashtable<Decl*> * &scope){
  return Type::nullType;
}

Operator::Operator(yyltype loc, const char *tok) : Node(loc) {
    Assert(tok != NULL);
    strncpy(tokenString, tok, sizeof(tokenString));
}

CompoundExpr::CompoundExpr(Expr *l, Operator *o, Expr *r) 
  : Expr(Join(l->GetLocation(), r->GetLocation())) {
    Assert(l != NULL && o != NULL && r != NULL);
    (op=o)->SetParent(this);
    (left=l)->SetParent(this); 
    (right=r)->SetParent(this);
}

CompoundExpr::CompoundExpr(Operator *o, Expr *r) 
  : Expr(Join(o->GetLocation(), r->GetLocation())) {
    Assert(o != NULL && r != NULL);
    left = NULL; 
    (op=o)->SetParent(this);
    (right=r)->SetParent(this);
}  

Type* AssignExpr::typeCheck(Hashtable<Decl*> * &scope){
  Type* lType = left->typeCheck(scope);
  Type* rType = right->typeCheck(scope);
  if(lType->getType() == namedType){
    if(rType->getType() != namedType &&
       !rType->IsEquivalentTo(Type::nullType)){
      ReportError::IncompatibleOperands(op, lType, rType);
    }
    if(!rType->IsEquivalentTo(Type::nullType) &&
       !rType->IsEquivalentTo(lType)){
      Decl* decl;
      Hashtable<Decl*> * cScope = scope;
      while(cScope != NULL){
	decl = cScope->Lookup(rType->checkType());
	if(decl == NULL){
	  cScope = cScope->getParent();
	} else {
	  break;
	}
      }
      if(decl){
	if(!decl->equalType(lType)){
	  ReportError::IncompatibleOperands(op, lType, rType);
	}
      }
    }
  } else if(!lType->IsEquivalentTo(Type::errorType) &&
	    !rType->IsEquivalentTo(Type::errorType)){
    if(rType->IsEquivalentTo(Type::nullType)){
    } else if(!lType->IsEquivalentTo(rType)){
      ReportError::IncompatibleOperands(op, lType, rType);
    }
  }
  return lType;
}

Type* ArithmeticExpr::typeCheck(Hashtable<Decl*> *&scope){
  Type* lType = Type::nullType;
  if(left){
    lType = left->typeCheck(scope);
  }
  Type* rType = right->typeCheck(scope);
  if(lType->IsEquivalentTo(Type::errorType) ||
     rType->IsEquivalentTo(Type::errorType)){
    return Type::errorType;
  } else {
    if(!((lType->IsEquivalentTo(Type::intType) ||
	  lType->IsEquivalentTo(Type::doubleType) ||
	  rType->IsEquivalentTo(Type::intType) || 
	  rType->IsEquivalentTo(Type::doubleType) ||
	  lType->IsEquivalentTo(Type::nullType)) && 
	 lType->IsEquivalentTo(rType))){
      ReportError::IncompatibleOperands(op, lType, rType);
    }
  }
  return rType;
}

Type* EqualityExpr::typeCheck(Hashtable<Decl*> * &scope){
  Type* lType = left->typeCheck(scope);
  Type* rType = right->typeCheck(scope);
  if(lType->getType() == namedType && rType->IsEquivalentTo(Type::nullType)){
  } else if(!lType->IsEquivalentTo(rType)){
    ReportError::IncompatibleOperands(op, lType, rType);
  }
  return Type::boolType;
}

Type* RelationalExpr::typeCheck(Hashtable<Decl*> * &scope){
  Type* lType = left->typeCheck(scope);
  Type* rType = right->typeCheck(scope);
  if(lType->IsEquivalentTo(rType)){
  } else {
    ReportError::IncompatibleOperands(op, lType, rType);
  }
  return Type::boolType;
}

Type* LogicalExpr::typeCheck(Hashtable<Decl*> * &scope){
  Type *lType, *rType;
  if(left){
    lType = left->typeCheck(scope);
  }
  rType = right->typeCheck(scope);
  if(left){
    if(!(lType->IsEquivalentTo(Type::boolType) &&
	 rType->IsEquivalentTo(Type::boolType))){
      ReportError::IncompatibleOperands(op, lType, rType);
    }
  } else {
    if(!rType->IsEquivalentTo(Type::boolType)){
      ReportError::IncompatibleOperand(op, rType);
    }
  }
  return Type::boolType;
}

ArrayAccess::ArrayAccess(yyltype loc, Expr *b, Expr *s) : LValue(loc) {
    (base=b)->SetParent(this); 
    (subscript=s)->SetParent(this);
}

Type* ArrayAccess::typeCheck(Hashtable<Decl*> * &scope){
  if(base->typeCheck(scope)->getType() != arrayType){
    ReportError::BracketsOnNonArray(base);
  }
  if(!subscript->typeCheck(scope)->IsEquivalentTo(Type::intType)){
    ReportError::SubscriptNotInteger(subscript);
  }
  return base->typeCheck(scope)->elemT();
}

Type* This::typeCheck(Hashtable<Decl*> * &scope){
  Decl* decl;
  Hashtable<Decl*> * cScope = scope;
  while(cScope != NULL){
    decl = cScope->Lookup("this");
    if(decl){
      return decl->typeCheck();
    } else {
      cScope = cScope->getParent();
    }
  }
  delete decl;
  delete cScope;
  ReportError::ThisOutsideClassScope(this);
  return Type::nullType;
}
     
FieldAccess::FieldAccess(Expr *b, Identifier *f) 
  : LValue(b? Join(b->GetLocation(), f->GetLocation()) : *f->GetLocation()) {
    Assert(f != NULL); // b can be be NULL (just means no explicit base)
    base = b;
    if (base) base->SetParent(this); 
    (field=f)->SetParent(this);
}


Type* FieldAccess::typeCheck(Hashtable<Decl*> * &scope){
  Decl* decl;
  Hashtable<Decl*> * cScope = scope;
  if(base){
    //Decl* thisdecl;
    Type* btype = base->typeCheck(scope);
    //find the base first
    while(cScope != NULL){
      decl = cScope->Lookup(btype->checkType());
      if(decl == NULL){
	cScope = cScope->getParent();
      } else {
	break;
      }
    }
    if(decl){
      Decl* f;
      //find the field in Class
      if((f = decl->scope->Lookup(field->getID()))){
	if (base->exprType() != 1){
	  ReportError::InaccessibleField(field, btype);
	}
	return f->typeCheck();
      } else {
	ReportError::FieldNotFoundInBase(field, btype);
      }
    } else {
      ReportError::FieldNotFoundInBase(field, btype);
    }
    return Type::errorType;    
  } else {
    while(cScope != NULL){
      decl = cScope->Lookup(field->getID());
      if(decl == NULL){
	cScope = cScope->getParent();
      } else {
	if(decl->getType() != varDecl){
	  ReportError::IdentifierNotDeclared(field, LookingForVariable);
	  return Type::errorType;
	} else {
	    return decl->typeCheck();
	}
      }
    }
    ReportError::IdentifierNotDeclared(field, LookingForVariable);
    return Type::errorType;
  }
}

Call::Call(yyltype loc, Expr *b, Identifier *f, List<Expr*> *a) : Expr(loc)  {
    Assert(f != NULL && a != NULL); // b can be be NULL (just means no explicit base)
    base = b;
    if (base) base->SetParent(this);
    (field=f)->SetParent(this);
    (actuals=a)->SetParentAll(this);
}

Type* Call::typeCheck(Hashtable<Decl*> * &scope){
  if(base){
    Type* btype = base->typeCheck(scope);
    //are you built in calls?
    if(btype->getType() == arrayType){
      if(strcmp(field->getID(), "length") == 0){
	if(actuals->NumElements() != 0){
	  ReportError::NumArgsMismatch(field, 0, actuals->NumElements());
	}
	return Type::intType;
      } else {
	ReportError::FieldNotFoundInBase(field, btype);
	return Type::errorType;
      }
    } else if (btype->getType() == namedType) {
      //search for the class first.
      Decl* decl;
      Hashtable<Decl*> * cScope = scope;
      while(cScope != NULL){
	decl = cScope->Lookup(btype->checkType());
	if(decl == NULL){
	  cScope = cScope->getParent();
	} else {
	  if(!decl->checked){
	    decl->Check(cScope);
	  }
	  break;
	}
      }
      //class exists:
      if(decl){
	//find method...?
	Decl* method = decl->scope->Lookup(field->getID());
	if(method){
	  List<Decl*> * actuallist = new List<Decl*>();
	  int n = 0;
	  while(true){
	    Decl* actual;
	    std::stringstream ss;
	    ss << n;
	    const char* nth = ss.str().c_str();
	    if((actual = method->scope->Lookup(nth))){
	      actuallist->Append(actual);
	      n++;
	    } else {
	      break;
	    }
	  }
	  if(actuallist->NumElements() != actuals->NumElements()){
	    ReportError::NumArgsMismatch(field, actuallist->NumElements(), actuals->NumElements());
	  }
	  for(int i = 0; i < actuals->NumElements() && i < actuallist->NumElements(); i++){
	    Type* a1 = actuallist->Nth(i)->typeCheck();
	    Type* a2 = actuals->Nth(i)->typeCheck(scope);
	    if(!a1->IsEquivalentTo(a2) && !a2->IsEquivalentTo(Type::nullType)){
	      ReportError::ArgMismatch(actuals->Nth(i), i+1, a2, a1);
	    }
	  }
	  return method->typeCheck();
	} else {
	  ReportError::FieldNotFoundInBase(field, btype);
	  for(int i = 0; i < actuals->NumElements(); i++){
	    Type* a = actuals->Nth(i)->typeCheck(scope);
	  }
	  return Type::errorType;
	}
      } else {
	//class does not exist...what??
	return Type::errorType;
      }
    } else {
      ReportError::FieldNotFoundInBase(field, btype);
      return Type::errorType;
    }
  } else {
    Decl* decl;
    Decl* extends;
    Hashtable<Decl*> * cScope = scope;
    while(cScope != NULL){
      decl = cScope->Lookup(field->getID());
      if(decl == NULL){
	cScope = cScope->getParent();
      } else {
	if(!decl->checked){
	  decl->Check(cScope);
	}
	break;
      }
    }
    if(!decl){
      cScope = scope;
      while(cScope != NULL){
	extends = cScope->Lookup("extends");
	if(extends){
	  decl = extends->scope->Lookup(field->getID());
	  break;
	} else {
	  cScope = cScope->getParent();
	}
      }
    }
    if(decl){
      //found function. Check for actuals now...
      List<Decl*> * actuallist = new List<Decl*>();
      int n = 0;
      while(true){
	Decl* actual;
	std::stringstream ss;
	ss << n;
	const char* nth = ss.str().c_str();\
	if((actual = decl->scope->Lookup(nth))){
	  actuallist->Append(actual);
	  n++;
	} else {
	  break;
	}
      }
      if(actuallist->NumElements() != actuals->NumElements()){
	ReportError::NumArgsMismatch(field, actuallist->NumElements(), actuals->NumElements());
      }
      for(int i = 0; i < actuals->NumElements() && i < actuallist->NumElements(); i++){
	Type* a1 = actuallist->Nth(i)->typeCheck();
	Type* a2 = actuals->Nth(i)->typeCheck(scope);
	if(!a1->IsEquivalentTo(a2)){
	  ReportError::ArgMismatch(actuals->Nth(i), i+1, a2, a1);
	}
      }
      return decl->typeCheck();
    } else {
      ReportError::IdentifierNotDeclared(field, LookingForFunction);
      for(int i = 0; i < actuals->NumElements(); i++){
	Type* a = actuals->Nth(i)->typeCheck(scope);
      }
      return Type::nullType;
    }
  }
  
  return Type::nullType;
}

NewExpr::NewExpr(yyltype loc, NamedType *c) : Expr(loc) { 
  Assert(c != NULL);
  (cType=c)->SetParent(this);
}

Type* NewExpr::typeCheck(Hashtable<Decl*> * &scope){
  Decl* decl;
  Hashtable<Decl*> * cScope = scope;
  while(cScope != NULL){
    decl = cScope->Lookup(cType->checkType());
    if(decl == NULL){
      cScope = cScope->getParent();
    } else {
      break;
    }
  }
  if(decl){
    if(decl->getType() != classDecl){
    ReportError::IdentifierNotDeclared(cType->getid(), LookingForClass);
    }
  } else {
    ReportError::IdentifierNotDeclared(cType->getid(), LookingForClass);
  }
  return cType;
}

NewArrayExpr::NewArrayExpr(yyltype loc, Expr *sz, Type *et) : Expr(loc) {
    Assert(sz != NULL && et != NULL);
    (size=sz)->SetParent(this); 
    (elemType=et)->SetParent(this);
}

Type* NewArrayExpr::typeCheck(Hashtable<Decl*> * &scope) { 
  struct yyltype temploc;
  if(!size->typeCheck(scope)->IsEquivalentTo(Type::intType)){
    ReportError::NewArraySizeNotInteger(size);
  }
  if(elemType->getType() == ptype){
    ArrayType* rType = new ArrayType(temploc, elemType); 
    return rType;
  } else if(elemType->getType() == namedType){
    Decl* decl;
    Hashtable<Decl*> * cScope = scope;
    while(cScope != NULL){
      decl = cScope->Lookup(elemType->checkType());
      if(decl == NULL){
	cScope = cScope->getParent();
      } else {
	break;
      }
    }
    if(decl){
      if(decl->getType() != classDecl){
	ReportError::IdentifierNotDeclared(elemType->getid(), LookingForType);
	ArrayType* rType = new ArrayType(temploc, Type::errorType);
	return rType;
      }
      ArrayType* rType = new ArrayType(temploc, elemType);
      return rType;
    } else {
	ReportError::IdentifierNotDeclared(elemType->getid(), LookingForType);
	ArrayType* rType = new ArrayType(temploc, Type::errorType);
	return rType;
    }
  } else {
    ArrayType* rType = new ArrayType(temploc, elemType);
    return rType;
  }
};
