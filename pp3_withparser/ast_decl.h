/* File: ast_decl.h
 * ----------------
 * In our parse tree, Decl nodes are used to represent and
 * manage declarations. There are 4 subclasses of the base class,
 * specialized for declarations of variables, functions, classes,
 * and interfaces.
 *
 * pp3: You will need to extend the Decl classes to implement 
 * semantic processing including detection of declaration conflicts 
 * and managing scoping issues.
 */

#ifndef _H_ast_decl
#define _H_ast_decl

#include "ast.h"
#include "list.h"
#include "hashtable.h"

class Type;
class NamedType;
class Identifier;
class Stmt;

typedef enum { pdecl, classDecl, interfaceDecl, fnDecl, varDecl } typeD;

class Decl : public Node 
{
  protected:
    Identifier *id;
  
  public:
    Decl(Identifier *name);
    friend std::ostream& operator<<(std::ostream& out, Decl *d) { return out << d->id; }
    virtual void Check(Hashtable<Decl*> * & parentScope){};
    const char* getID(){ return id->getID(); };
    virtual Type* typeCheck(){ return NULL; };
    virtual bool equalType(Type* other) { return false; };
    Hashtable<Decl *> * scope;
    virtual typeD getType(){ return pdecl; }
    virtual void printout();
    virtual Identifier* getid() {return id;};
};

class VarDecl : public Decl
{
  protected:
    Type *type;
    
  public:
    VarDecl(Identifier *name, Type *type);
    void Check(Hashtable<Decl*> * & parentScope);
    Type* typeCheck() { return type; };
    bool equalType(Type* other);
    typeD getType(){ return varDecl; }
};

class ClassDecl : public Decl 
{
  protected:
    List<Decl*> *members;
    NamedType *extends;
    List<NamedType*> *implements;

  public:
    ClassDecl(Identifier *name, NamedType *extends, 
              List<NamedType*> *implements, List<Decl*> *members);
    void Check(Hashtable<Decl*> * & parentScope);
    Type* typeCheck();
    bool equalType(Type* other);
    typeD getType(){ return classDecl; }
};

class InterfaceDecl : public Decl 
{
  protected:
    List<Decl*> *members;
    
  public:
    InterfaceDecl(Identifier *name, List<Decl*> *members);
    void Check(Hashtable<Decl*> * & parentScope);
    typeD getType(){ return interfaceDecl; }
};

class FnDecl : public Decl 
{
  protected:
    List<VarDecl*> *formals;
    Type *returnType;
    Stmt *body;
    
  public:
    FnDecl(Identifier *name, Type *returnType, List<VarDecl*> *formals);
    void SetFunctionBody(Stmt *b);
    void Check(Hashtable<Decl*> * & parentScope);
    Type *typeCheck() { return returnType; }
    typeD getType(){ return fnDecl; }
};

#endif
